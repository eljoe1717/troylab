<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    public static  function boot()
    {
        parent::boot();
        self::created(function($model){
            $students = $model->school->students->sortBy('name');
            $order = 1;
            $students->transform(function ($q)use(&$order){
                $q->order = $order;
                $q->save();
                ++$order;
            });
        });

        self::updated(function($model){
            $changes = array_keys($model->changes ?? []);
            $count = count(array_intersect(['deleted_at','name'],$changes));
            if ($count > 0) {
                $students = $model->school->students->sortBy('name');
                $order = 1;
                $students->transform(function ($q)use(&$order){
                    $q->order = $order;
                    $q->save();
                    ++$order;
                });
            }
        });

        self::saved(function($model){
            $changes = array_keys($model->changes ?? []);
            $count = count(array_intersect(['deleted_at','name'],$changes));
            if ($count > 0) {
                $students = $model->school->students->sortBy('name');
                $order = 1;
                $students->transform(function ($q)use(&$order){
                    $q->order = $order;
                    $q->save();
                    ++$order;
                });
            }
        });


        self::deleted(function($model){
            $students = $model->school->students->sortBy('name');
            $order = 1;
            $students->transform(function ($q)use(&$order){
                $q->order = $order;
                $q->save();
                ++$order;
            });
        });
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
