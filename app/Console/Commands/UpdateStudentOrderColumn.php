<?php

namespace App\Console\Commands;

use App\Mail\StudentOrderingMail;
use App\School;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class UpdateStudentOrderColumn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-students:order-column';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        School::with('students')->get()->map(function ($school){
            $order = 1;
            $school->students->transform(function ($q)use(&$order){
                $q->order = $order;
                $q->save();
                ++$order;
            });
        });

        Mail::to('eljoe1717@gmail.com')
            ->send(new StudentOrderingMail());
    }
}
