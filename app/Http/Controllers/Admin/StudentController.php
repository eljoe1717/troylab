<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\School;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function show($id)
    {
        $data['school'] = $school = School::find($id);
        if (!$school) {
            return redirect('schools');
        }

        $data['students'] = $school->students()->paginate(5);
        return view('admin.students.index',$data);
    }

    public function edit($id)
    {
        $data['student'] = Student::find($id);
        $data['action'] = route('admin.students.update',$data['student']->id);
        $data['method'] = 'PUT';
        return view('admin.students.form',$data);
    }

    public function update($id,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required'
        ]);

        if ($validation->fails()) {
            return back()->with('error',$validation->errors()->first());
        }

        $student = Student::find($id);

        $student->update([
            'name'=>$request->name,
            'status'=>$request->status == 'on'
        ]);
        return redirect(url()->previous())->with('success','Updated Successfully!');
    }

    public function create()
    {
        $data['action'] = route('admin.students.store');
        $data['schools'] = School::pluck('name','id');
        $data['method'] = 'POST';
        return view('admin.students.form',$data);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required',
            'school_id'=>'required|exists:schools,id'
        ]);

        if ($validation->fails()) {
            return redirect(url()->previous())->with('error',$validation->errors()->first());
        }
        Student::create([
            'name'=>$request->name,
            'status'=>$request->status == 'on',
            'school_id'=>$request->school_id
        ]);

        return redirect(url()->previous())->with('success','Created Successfully!');
    }

    
    public function destroy($id)
    {
        Student::find($id)->delete();
        return response()->json(['status'=>true]);
    }
}
