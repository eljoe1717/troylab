<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SchoolController extends Controller
{
    public function index()
    {
        $data['schools'] = School::/*->withTrashed()*/paginate(5);
        return view('admin.schools.index',$data);
    }

    public function edit($id)
    {
        $data['school'] = School::find($id);
        $data['action'] = route('admin.schools.update',$data['school']->id);
        $data['method'] = 'PUT';
        return view('admin.schools.form',$data);
    }

    public function update($id,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required'
        ]);

        if ($validation->fails()) {
            return back()->with('error',$validation->errors()->first());
        }

        $school = School::find($id);

        $school->update([
            'name'=>$request->name,
            'status'=>$request->status == 'on'
        ]);
        return back()->with('success','Updated Successfully!');
    }

    public function create()
    {
        $data['action'] = route('admin.schools.store');
        $data['method'] = 'POST';
        return view('admin.schools.form',$data);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required'
        ]);

        if ($validation->fails()) {
            return back()->with('error',$validation->errors()->first());
        }
        School::create([
            'name'=>$request->name,
            'status'=>$request->status == 'on'
        ]);

        return back()->with('success','Created Successfully!');
    }

    public function destroy($id)
    {
        School::find($id)->delete();
        return response()->json(['status'=>true]);
    }
}
