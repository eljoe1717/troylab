<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SchoolCollection;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;
use App\School;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function show($id)
    {
        $school = School::find($id);
        if (!$school) {
            return response()->json('Page Not Found',404);
        }

        $students = $school->students()->paginate(10);
        return response()->json(new StudentCollection($students));
    }

    public function update($id,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required',
            'school_id'=>'required|exists:schools,id'
        ]);

        if ($validation->fails()) {
            return response()->json(['msg'=>$validation->errors()->first()],422);
        }

        $student = Student::find($id);
        $student->update($request->only('name','status','school_id'));
        return response()->json(new StudentResource($student));
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required',
            'school_id'=>'required|exists:schools,id'
        ]);

        if ($validation->fails()) {
            return response()->json(['msg'=>$validation->errors()->first()],422);
        }
        $student = Student::create([
            'name'=>$request->name,
            'status'=>$request->status??0,
            'school_id'=>$request->school_id
        ]);

        return response()->json(new StudentResource($student));
    }

    
    public function destroy($id)
    {
        $student = Student::find($id);
        if (!$student) {
            return response()->json('not found',404);
        }
        $student->delete();
        return response()->json(['status'=>true]);
    }

    public function schools()
    {
        $schools = School::paginate(10);
        return response()->json(new SchoolCollection($schools));
    }
}
