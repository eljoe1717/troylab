<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class StudentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data['data'] = $this->collection->transform(function($student){
            return [
                'id'=>$student->id,
                'name'=>$student->name,
                'school_id'=>$student->school_id,
                'status'=>$student->status,
                'created_at'=>$student->created_at
            ];
        });

        if ($this->collection instanceof LengthAwarePaginator) {
            $data['paginate'] = [
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ];
        }
        return $data;
    }
}
