<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Troylab | @yield('title')</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    @stack('styles')
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('admin.schools.index')}}">
                <img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="30" height="24"
                    class="d-inline-block align-text-top">
                Bootstrap
            </a>
        </div>
    </nav>

    <div class="container">
        @if(session('error'))
            <div class="col-sm-12 text-center alert alert-danger">
                {{$error}}
            </div>
        @endif
        @if(session('success'))
        <div class="col-sm-12 text-center alert alert-success">
            {{session('success')}}
        </div>
    @endif

        @yield('content')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    @stack('scripts')
</body>

<script>
    $('.deleteBTN').click(function () {
        var url = $(this).data('href');

        $.ajax({
            url,
            method : 'POST',
            data : {_method:'DELETE','_token':"{{csrf_token()}}"},
            success : ()=>window.location.reload()
        })
    });
</script>
</html>
