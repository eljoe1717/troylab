@extends('admin.layout')

@section('title',isset($school->id) ? "Update School#".$school->id : 'Create School')
@section('content')
    <form action="{{$action}}" method="POST">
        @csrf
        @if ($method == 'PUT')
            @method('PUT')            
        @endif
        <div class="row">
            <div class="col-sm-6">
                <label for="name">Name :</label>
                <input type="text" name="name" class="form-control" value="{{isset($school) ? $school->name : ''}}">
            </div>
            <div class="form-check col-sm-3" style="margin-top: 26px" >
                <label for="flexCheckChecked">
                    Checked checkbox
                </label>
                <input  name="status" type="checkbox" id="flexCheckChecked" {{isset($school) && $school->status == 1 ? 'checked' : ''}}>
            </div>

            <button type="submit" class="col-sm-3 btn btn-info">Save</button>
        </div>
    </form>
@endsection