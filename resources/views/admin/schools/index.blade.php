@extends('admin.layout')
@section('title','Schools')


@section('content')
<a href="{{route('admin.schools.create')}}" class="btn btn-success">New School</a>
<table class="table table-striped table-dark table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Status</th>
        <th scope="col">OPS</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($schools as $school)
        <tr>
            <th scope="row">{{$school->id}}</th>
            <td>{{$school->name}}</td>
            <td>{{$school->status == 1 ? 'ACTIVE' : "NOT ACTIVE"}}</td>
            <td>
                <a href="{{route('admin.schools.edit',$school->id)}}" class="btn btn-success">Update</a>
                <a href="javascript:" data-href="{{route('admin.schools.destroy',$school->id)}}" class="btn btn-danger deleteBTN">Delete</a>
                <a href="{{route('admin.students.show',$school->id)}}" class="btn btn-info">Show Students</a>
            </td>
          </tr>    
        @endforeach
    </tbody>
  </table>
  <div class="d-flex justify-content-center">
  {!! $schools->links() !!}
  </div>
@endsection