@extends('admin.layout')

@section('title',isset($student->id) ? "Update Student#".$student->id : 'Create Student')
@section('content')
<div class="row alert alert-primary">
    <a href="{{route('admin.students.show',request('school')??$student->school_id??'school')}}" class="btn btn-success">Back</a>
</div>
    <form action="{{$action}}" method="POST">
        @csrf
        @if ($method == 'PUT')
            @method('PUT')            
        @endif
        <div class="row">
            <div class="col-sm-6">
                <label for="name">Name :</label>
                <input type="text" name="name" class="form-control" value="{{isset($student) ? $student->name : ''}}">
            </div>
            @if (request('school'))
                <input type="hidden" name="school_id" value="{{request('school')}}">
            @elseif(isset($schools))
            <div class="col-sm-3">
                <label for="school_id">Select School</label>
                <select name="school_id" id="school_id" class="form-control">
                    @foreach ($schools as $id=>$name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="form-check col-sm-3" style="margin-top: 26px" >
                <label for="flexCheckChecked">
                    Checked checkbox
                </label>
                <input  name="status" type="checkbox" id="flexCheckChecked" {{isset($student) && $student->status == 1 ? 'checked' : ''}}>
            </div>

            <button type="submit" class="col-sm-3 btn btn-info">Save</button>
        </div>
    </form>
@endsection