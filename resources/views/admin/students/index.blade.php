@extends('admin.layout')
@section('title','Students')


@section('content')
<div class="row alert alert-primary">
    <h1 class="text-center">
        School : {{$school->name}}
    </h1>
    <a href="{{route('admin.schools.index')}}" class="btn btn-success">Back</a>
</div>

<a href="{{route('admin.students.create',['school'=>$school->id])}}" class="btn btn-success">Create New Student</a>
<table class="table table-striped table-dark table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Status</th>
        <th scope="col">OPS</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($students as $student)
        <tr>
            <th scope="row">{{$student->id}}</th>
            <td>{{$student->name}}</td>
            <td>{{$student->status == 1 ? 'ACTIVE' : "NOT ACTIVE"}}</td>
            <td>
              <a href="{{route('admin.students.edit',$student->id)}}" class="btn btn-info">Edit</a>
              <a href="javascript:" data-href="{{route('admin.students.destroy',$student->id)}}" class="btn btn-danger deleteBTN">Delete</a>
            </td>
          </tr>    
        @endforeach
    </tbody>
  </table>
  <div class="d-flex justify-content-center">
  {!! $students->links() !!}
</div>
  
@endsection