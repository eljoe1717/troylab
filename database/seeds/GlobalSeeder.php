<?php

use App\School;
use App\Student;
use App\User;
use Illuminate\Database\Seeder;

class GlobalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(School::class,10)->create()->each(function ($school) {
            $school->students()->createMany(factory(Student::class,10)->make()->toArray());
        });

        User::create([
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt(123456)
        ]);
    }
}
