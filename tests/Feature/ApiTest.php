<?php

namespace Tests\Feature;

use App\School;
use App\Student;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginApi()
    {
        $user = factory(User::class)->create();
 
        $response = $this->postJson('/api/login',[
            'email'=>$user->email,
            'password'=>123456
        ]);

        $response->assertStatus(200);
    }

    public function testShowStudentsApi()
    {
        $user = factory(User::class)->create();
        $school = factory(School::class)->create();
        $response = $this->actingAs($user,'api')->getJson('/api/students/'.$school->id);
        $response->assertStatus(200);
    }

    public function testCreateStudentsApi()
    {
        $user = factory(User::class)->create();
        $school = factory(School::class)->create();
        $student = factory(Student::class)->create([
            'school_id'=>$school->id
        ]);
        $response = $this->actingAs($user,'api')->postJson('/api/students/create',[
            'name'=>$student->name,
            'status'=>$student->status,
            'school_id'=>$student->school_id
        ]);
        $response->assertStatus(200);
    }

    public function testUpdateStudentApi()
    {
        $school = factory(School::class)->create();
        $user = factory(User::class)->create();
        $student = factory(Student::class)->create([
            'school_id'=>$school->id
        ]);
        $response = $this->actingAs($user,'api')->postJson('/api/students/'.$student->id.'/edit',[
            'name'=>$student->name,
            'status'=>$student->status??0,
            'school_id'=>$school->id
        ]);
        $response->assertStatus(200);
    }

    public function testDeleteStudentApi()
    {
        $user = factory(User::class)->create();
        $school = factory(School::class)->create();
        $student = factory(Student::class)->create([
            'school_id'=>$school->id
        ]);
        $response = $this->actingAs($user,'api')->postJson('/api/students/'.$student->id.'/delete');

        $response->assertStatus(200);
    }

    public function testGetAllSchoolsApi()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user,'api')->getJson('/api/schools');

        $response->assertStatus(200);
    }


}
