<?php

namespace Tests\Feature;

use App\School;
use App\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testShowSchools()
    {
        $response = $this->get('/schools');

        $response->assertStatus(200);
    }

    public function testCreateSchool()
    {
        $school = factory(School::class,1)->make();
        $this->assertTrue(true);
    }

    public function testDeleteSchool()
    {
        $school = factory(School::class)->make();
        $this->assertDeleted($school);
    }

    public function testShowAndCreateStudents()
    {
        $school = factory(School::class)->create();
        $school->students()->createMany(factory(Student::class,10)->make()->toArray());
        $response = $this->get('/students/'.$school->id);
        $response->assertStatus(200);
    }

    public function testDeleteStudent()
    {
        $school = factory(School::class)->create();
        $student = factory(Student::class)->make();
        $this->assertDeleted($student);
    }
}
