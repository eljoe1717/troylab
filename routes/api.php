<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('login','AuthController@apiLogin')->name('login');
Route::group(['middleware'=>'auth:api','namespace'=>'Api'],function (){
    Route::get('students/{id}', 'StudentController@show');
    Route::post('students/create', 'StudentController@store');
    Route::post('students/{id}/edit', 'StudentController@update');
    Route::post('students/{id}/delete', 'StudentController@destroy');
    Route::get('schools','StudentController@schools');
});
