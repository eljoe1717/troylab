<?php

use App\School;
use App\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','schools');
Route::group(['as'=>'admin.','namespace'=>'Admin'],function(){
    Route::resource('schools','SchoolController');
    Route::resource('students','StudentController');
});
